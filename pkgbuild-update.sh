#!/bin/bash

path="$1"
package="$2"
tag="$3"

git clone https://gitlab.com/archlinux-phalcon/packages/$package.git

current=`sed -n 's/pkgver=\(.*\)/\1/p' < $package/PKGBUILD`
curl --silent https://api.github.com/repos/$path/tags | jq -c '.[]|[.name] | @tsv' |
while IFS=$'\t' read -r name; do
	release=$(echo $name | cut -c3- | rev | cut -c2- | rev)
		if [[ "$release" == "$tag"* ]]; then
			if [ "$release" != "$current" ]; then
				hash=`curl -sL https://github.com/$path/archive/v$release.zip | sha256sum | cut -d ' ' -f 1`
				sed -i "/pkgver=/c\pkgver=$release" $package/PKGBUILD
				sed -i "/pkgrel=/c\pkgrel=1" $package/PKGBUILD
				sed -i "/sha256sums=/c\sha256sums=('$hash')" $package/PKGBUILD	
				git --git-dir $package/.git --work-tree=$package add PKGBUILD
				git --git-dir $package/.git --work-tree=$package commit -m "$package $release"
				git --git-dir $package/.git --work-tree=$package remote set-url --push origin https://$GIT_ACCESS_USER:$GIT_ACCESS_TOKEN@gitlab.com/archlinux-phalcon/packages/$package.git
				git --git-dir $package/.git --work-tree=$package push -u origin master
			fi
			break
		fi
done

rm -rf $package
